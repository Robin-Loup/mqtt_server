function DecoderToString(payloadb64) {
    payloadhex = Buffer.from(payloadb64, 'base64').toString('hex');
    return Decoder(payloadhex);
}

function signed16(e) {
    return 32768 & e && (e -= 65536), e
}

function signed8(e) {
    return 128 & e && (e -= 256), e
}

function float(e) {
    var a, t = 0 === (t = (2139095040 & e) >> 23) ? (a = !0, -149) : (a = !1, t - 127 - 23),
        r = 8388607 & e;
    return !1 === a && (r |= 8388608), 2147483648 & e ? -Math.pow(2, t) * r : Math.pow(2, t) * r
}

function Decoder(e, a) {
    return LoraWANEwattchDecoder(e, a, !0)
}
String.prototype.padStart || (String.prototype.padStart = function(e, a) {
    return e >>= 0, a = String(void 0 !== a ? a : " "), this.length > e ? String(this) : ((e -= this.length) > a.length && (a += a.repeat(e / a.length)), a.slice(0, e) + String(this))
}), LoraWANEwattchDecoder = function(e, a, t) {
    var r = {
            "00": "environnement",
            "01": "presence",
            "02": "ambiance",
            "08": "squid",
            10: "impulse",
            20: "tyness"
        },
        v = {
            "00": [2, function(e, a, t) {
                return {
                    uuid: "temperature_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "temperature",
                    value: signed16(e[0] | e[1] << 8) / 100,
                    unit: "°C"
                }
            }],
            "04": [1, function(e, a, t) {
                return {
                    uuid: "humidity_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "humidity",
                    value: e[0] / 2,
                    unit: "%RH"
                }
            }],
            "08": [2, function(e, a, t) {
                return {
                    uuid: "C02_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "co2",
                    value: e[0] | e[1] << 8,
                    unit: "ppm"
                }
            }],
            "0C": [2, function(e, a, t) {
                return {
                    uuid: "counter_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "counter",
                    value: e[0] | e[1] << 8,
                    unit: ""
                }
            }],
            "0E": [3, function(e, a, t) {
                return {
                    uuid: "time_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "time",
                    value: e[0] | e[1] << 8 | e[2] << 16,
                    unit: "s"
                }
            }],
            10: [2, function(e, a, t) {
                return {
                    uuid: "luminosity_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "luminosity",
                    value: e[0] | e[1] << 8,
                    unit: "lx"
                }
            }],
            14: [2, function(e, a, t) {
                return {
                    uuid: "motion_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "motion",
                    value: 10 * (e[0] | e[1] << 8),
                    unit: "s"
                }
            }],
            16: [2, function(e, a, t) {
                return {
                    uuid: "pressure_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "pressure",
                    value: signed16(e[0] | e[1] << 8) / 100,
                    unit: "barg"
                }
            }],
            18: [2, function(e, a, t) {
                return {
                    uuid: "flow_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "flow",
                    value: (e[0] | e[1] << 8) / 100,
                    unit: "m3/h"
                }
            }],
            "1A": [2, function(e, a, t) {
                return {
                    uuid: "volume_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "volume",
                    value: e[0] | e[1] << 8,
                    unit: "m3"
                }
            }],
            "1C": [4, function(e, a, t) {
                return {
                    uuid: "volume_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "volume",
                    value: float(e[0] | e[1] << 8 | e[2] << 16 | e[3] << 24),
                    unit: "m3"
                }
            }],
            20: [1, function(e, a, t) {
                return n(e, a, t, 1)
            }],
            22: [1, function(e, a, t) {
                return n(e, a, t, 2)
            }],
            24: [1, function(e, a, t) {
                return n(e, a, t, 4)
            }],
            26: [1, function(e, a, t) {
                return n(e, a, t, 8)
            }],
            28: [3, function(e, a, t) {
                var r, n, u;
                r = (224 & e[0]) >> 5 ? .001 : .01;
                0 == (31 & e[0]) ? (n = "mA", u = "4:20") : 1 == (31 & e[0]) ? (n = "V", u = "0:10") : 2 == (31 & e[0]) && (n = "V", u = "0:24");
                return {
                    uuid: "analog_input_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t,
                        scale: u
                    },
                    type: "analogInput",
                    value: signed16(e[1] | e[2] << 8) * r,
                    unit: n
                }
            }],
            40: [0, function(e, a, t) {
                var r = 0,
                    n = (240 & e[r]) >> 4,
                    u = 15 & e[r],
                    i = [];
                switch (r++, u) {
                    case 0:
                        for (var s = 0; s < n; s++) i.push({
                            type: "currentIndex",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: 10 * (e[r] | e[r + 1] << 8 | e[r + 2] << 16),
                            unit: "mAh"
                        }), r += 3;
                        break;
                    case 1:
                        for (s = 0; s < n; s++) i.push({
                            type: "current",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16,
                            unit: "mA"
                        }), r += 3;
                        break;
                    case 2:
                        for (s = 0; s < n; s++) i.push({
                            type: "currentIndex",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: 10 * (e[r] | e[r + 1] << 8 | e[r + 2] << 16),
                            unit: "mAh"
                        }), r += 3;
                        for (s = 0; s < n; s++) i.push({
                            type: "current",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16,
                            unit: "mA"
                        }), r += 3;
                        break;
                    case 3:
                        for (s = 0; s < n; s++) i.push({
                            type: "activeEnergyIndex",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: 10 * (e[r] | e[r + 1] << 8 | e[r + 2] << 16),
                            unit: "Wh"
                        }), r += 3;
                        for (s = 0; s < n; s++) i.push({
                            type: "positiveReactiveEnergyIndex",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: 10 * (e[r] | e[r + 1] << 8 | e[r + 2] << 16),
                            unit: "varh"
                        }), r += 3;
                        for (s = 0; s < n; s++) i.push({
                            type: "negativeReactiveEnergyIndex",
                            hardwareData: {
                                socket: a,
                                channel: t + s
                            },
                            uuid: "clamp_s" + a + "_c" + (t + s),
                            value: 10 * (e[r] | e[r + 1] << 8 | e[r + 2] << 16),
                            unit: "varh"
                        }), r += 3
                }
                return e.splice(0, r), i
            }],
            44: [4, function(e, a, t) {
                return {
                    uuid: "active_energy_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "activeEnergyIndex",
                    value: e[0] | e[1] << 8 | e[2] << 16 | e[3] << 24,
                    unit: "kWh"
                }
            }],
            46: [4, function(e, a, t) {
                return {
                    uuid: "reactive_energy_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "reactiveEnergyIndex",
                    value: e[0] | e[1] << 8 | e[2] << 16 | e[3] << 24,
                    unit: "kvarh"
                }
            }],
            48: [0, function(e, a, t) {
                for (var r = 0, n = [], u = 0; u < 12; u++) n.push({
                    type: "currentIndex",
                    hardwareData: {
                        socket: a,
                        channel: t + u
                    },
                    uuid: "clamp_s" + a + "_c" + (t + u),
                    value: 10 * (e[r] | e[r + 1] << 8 | e[r + 2] << 16),
                    unit: "mAh"
                }), r += 3;
                return e.splice(0, r), n
            }],
            50: [0, function(e, a, t) {
                var r = 0,
                    n = {
                        uuid: "tic_s" + a + "_c" + t,
                        hardwareData: {
                            socket: a,
                            channel: t
                        },
                        type: "tic",
                        values: [],
                        metadata: {}
                    },
                    u = e[r];
                r++;
                var i, s = 0,
                    c = 0;
                15 == (15 & u) || 14 == (15 & u) || 0 === u ? (n.metadata.counterError = p[u.toString(16).toUpperCase().padStart(2, "0")] || "error code 0x" + u.toString(16).padStart(2, "0"), e.splice(0, r)) : n.metadata.counterType = p[u.toString(16).toUpperCase().padStart(2, "0")];
                var o, d = [];
                switch (u) {
                    case 16:
                        switch (n.metadata.checksum = e[1], i = e[2], s = (224 & i) >> 5 & 7, c = 31 & i, n.metadata.subscription = h[s.toString(16).toUpperCase().padStart(2, "0")], n.metadata.tarrifPeriod = H[c.toString(16).toUpperCase().padStart(2, "0")], r += 2, s) {
                            case 0:
                                d = ["Base"];
                                break;
                            case 1:
                                d = ["HC", "HP"];
                                break;
                            case 2:
                                d = ["Heures Normales", "Heures Pointe mobile"];
                                break;
                            case 3:
                                d = ["HC Jour bleu", "HC Jour blanc", "HC Jour rouge", "HP Jour bleu", "HP Jour blanc", "HP Jour rouge"]
                        }
                        for (var l = 0; l < d.length; l++) o = d[l], n.values.push({
                            type: o,
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16 | e[r + 3] << 24,
                            unit: "Wh"
                        }), r += 4;
                        break;
                    case 32:
                        i = e[r], s = (240 & i) >> 4 & 15, c = 15 & i, n.metadata.subscription = P[s.toString(16).toUpperCase().padStart(2, "0")], n.metadata.tarrifPeriod = f[c.toString(16).toUpperCase().padStart(2, "0")], r++, d = ["1", "2", "3", "4"];
                        for (l = 0; l < d.length; l++) o = d[l], n.values.push({
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16,
                            unit: "kWh"
                        }), r += 3;
                        break;
                    case 48:
                        s = e[r], n.metadata.subscription = g[s.toString(16).toUpperCase().padStart(2, "0")], c = e[++r], n.metadata.tarrifPeriod = y[c.toString(16).toUpperCase().padStart(2, "0")], r++, d = ["ActiveEnergyCurrentPeriode", "PositiveReactiveEnergyCurrentPeriode", "NegativeReactiveEnergyCurrentPeriode", "ActiveEnergyPreviousPeriode", "PositiveReactiveEnergyPreviousPeriode", "NegativeReactiveEnergyPreviousPeriode"];
                        for (l = 0; l < d.length; l++) o = d[l], n.values.push({
                            type: o,
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16,
                            unit: l % 3 == 0 ? "kWh" : "kVarh"
                        }), r += 3;
                        break;
                    case 64:
                        switch (s = e[r], n.metadata.subscription = k[s.toString(16).toUpperCase().padStart(2, "0")], c = e[++r], n.metadata.tarrifPeriod = C[c.toString(16).toUpperCase().padStart(2, "0")], r++, n.values.activePower10Minutes = {
                                value: e[r] | e[r + 1] << 8,
                                unit: "kW"
                            }, r += 2, n.values.reactivePower10Minutes = {
                                value: e[r] | e[r + 1] << 8,
                                unit: "kVar"
                            }, r += 2, s) {
                            case 10:
                                d = ["P", "HPH", "HCH", "HPE", "HCE"];
                                break;
                            case 11:
                                d = ["P", "HPH", "HCH", "HPD", "HCD", "HPE", "HCE", "JA"];
                                break;
                            case 16:
                                d = ["PM", "HH", "HPE", "HCE"];
                                break;
                            case 17:
                                d = ["PM", "HH", "HD", "HPE", "HCE", "JA"];
                                break;
                            case 18:
                                d = ["PM", "HM", "DSM", "SCM"]
                        }
                        for (l = 0; l < d.length; l++) o = d[l], n.values.push({
                            type: o,
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16,
                            unit: "kWh"
                        }), r += 3;
                        break;
                    case 80:
                        n.metadata.checksum = e[r], c = e[++r], n.metadata.tarrifPeriod = 31 & c, r++, n.values.push({
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16 | e[r + 3] << 24,
                            unit: "Wh"
                        }), r += 4;
                        break;
                    case 81:
                        n.metadata.checksum = e[r], c = e[++r], n.metadata.tarrifPeriod = 31 & c, r++, n.values.push({
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16 | e[r + 3] << 24,
                            unit: "Wh"
                        }), r += 4, n.values.push({
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16 | e[r + 3] << 24,
                            unit: "Wh"
                        }), r += 4;
                        break;
                    case 96:
                        n.metadata.checksum = e[r], r++, n.metadata.tarrifPeriod = String.fromCharCode(e[r]) + String.fromCharCode(e[r + 1]) + String.fromCharCode(e[r + 2]), r += 3, n.values.push({
                            type: "tgphis",
                            value: signed16(e[r] | e[r + 1] << 8) / 100,
                            unit: ""
                        }), r += 2, d = ["1", "2", "3", "4", "5", "6", "7", "8"];
                        for (l = 0; l < d.length; l++) o = d[l], n.values.push({
                            value: e[r] | e[r + 1] << 8 | e[r + 2] << 16 | e[r + 3] << 24,
                            unit: "Wh"
                        }), r += 4
                }
                return e.splice(0, r), n
            }],
            "5C": [0, function(e, a, t) {
                var r = 0,
                    n = {
                        uuid: "modbus_s" + a + "_c" + t,
                        hardwareData: {
                            socket: a,
                            channel: t
                        },
                        type: "modbus",
                        values: [],
                        metadata: {}
                    },
                    u = e[++r];
                n.metadata.application_code = 63 & u, n.metadata.standardised = (64 & u) >> 7 == 1;
                var i = e[++r];
                switch (i) {
                    case 3:
                        n.metadata.command = "Read Holding Registers";
                        break;
                    case 4:
                        n.metadata.command = "Read Input Registers";
                        break;
                    case 8:
                        n.metadata.command = "Read Diagnostics";
                        break;
                    case 16:
                        n.metadata.command = "Write Multiples Registers Response";
                        break;
                    case 80:
                        n.metadata.command = "Master Write Multiples Registers Response";
                        break;
                    default:
                        n.metadata.command = "Unknown command : " + (127 & i)
                }
                if (r++, 3 === i || 4 === i) {
                    n.metadata.startAdress = (e[r + 1] << 8) + e[r], r += 2, n.metadata.wordCount = e[r] / 2, r++;
                    for (var s = [], c = 0; c < n.metadata.wordCount && !(r + 1 > e.length);) s.push((e[r + 1] << 8) + e[r]), r += 2, c++;
                    n.values = s
                }
                return e.splice(0, r), n
            }],
            74: [1, function(e, a, t) {
                return {
                    uuid: "battery_s" + a + "_c" + t,
                    hardwareData: {
                        socket: a,
                        channel: t
                    },
                    type: "battery",
                    value: e,
                    unit: ""
                }
            }]
        },
        m = {
            "00": [1, function(e, a, t) {
                return {
                    value: r[e[0].toString(16).toUpperCase().padStart(2, "00")],
                    type: "nodeType"
                }
            }],
            "02": [2, function(e, a, t) {
                return {
                    value: e[1] + "." + e[0],
                    type: "version"
                }
            }],
            "04": [1, function(e, a, t) {
                return {
                    value: e[0],
                    type: "batteryLevel"
                }
            }],
            "08": [2, function(e, a, t) {
                return {
                    value: 10 * (e[0] | e[1] << 8),
                    type: "periodicity"
                }
            }]
        };

    function n(e, a, t, r) {
        for (var n = [], u = e[0], i = 0; i < r; i++) n.push({
            uuid: "digital_input_s" + a + "_c" + (t + i),
            hardwareData: {
                socket: a,
                channel: t + i
            },
            type: "boolean",
            value: 1 & u,
            unit: ""
        }), u >>= 1;
        return e.splice(0, 2), n
    }
    this.decode = function(e, a, t) {
        if ("string" == typeof e) {
            for (var r = [], n = 0, u = e.length; n < u; n += 2) r.push(parseInt(e.substr(n, 2), 16));
            e = r
        }
        null == t && (t = !0);
        var i = 0,
            s = [];
        if (t) {
            i = 0 === e[0] ? 0 : 1;
            if (e[1] !== e.length - 2) return s.push({
                type: "error",
                value: "Payload size indicated does not match payload size given"
            }), {
                data: s
            };
            e.splice(0, 2)
        }
        for (; 0 < e.length;) {
            var c, o, d = 1 & e[0],
                l = (254 & e[0]).toString(16).toUpperCase().padStart(2, "00");
            if (null == (c = 0 === i ? v[l] : m[l])) return s.push({
                type: "error",
                value: "unknown object type : " + l
            }), {
                data: s
            };
            e.splice(0, 1);
            var p = 0,
                h = 0;
            0 === i && d && (p = (224 & e[0]) >> 5, h = 31 & e[0], e.splice(0, 1)), o = c[1](e, p, h), Array.isArray(o) ? s = s.concat(o) : s.push(o), e.splice(0, c[0])
        }
        return {
            data: s
        }
    };
    var p = {
            "0E": "Données erronées",
            "0F": "Compteur Inconnu",
            10: "Bleu",
            "1F": "Bleu avec defaut",
            20: "Jaune",
            "2F": "Jaune avec defaut",
            30: "PME-PMI",
            "3F": "PME-PMI avec defaut",
            40: "ICE",
            "4E": "ICE V2.4",
            "4F": "ICE avec defaut",
            50: "Linky",
            51: "Linky Injection",
            "5F": "Linky avec defaut",
            60: "Saphir",
            "6F": "Saphir avec defaut"
        },
        h = {
            "00": "Base",
            "01": "HP/HC",
            "02": "EJP",
            "03": "TEMPO"
        },
        H = {
            "00": "Base",
            "01": "HC",
            "02": "HP",
            "03": "EJP heures normales",
            "04": "EJP heures de pointe mobile",
            "05": "TEMPO HC Jour Bleu",
            "06": "TEMPO HC Jour Blanc",
            "07": "TEMPO HC Jour Rouge",
            "08": "TEMPO HP Jour Bleu",
            "09": "TEMPO HP Jour Blanc",
            "0a": "TEMPO HP Jour Rouge"
        },
        P = {
            "01": "Ete",
            "02": "Hiver",
            "03": "Pointe mobile"
        },
        f = {
            "01": "Heures Pleines",
            "02": "Heures Creuses",
            "03": "Heures de pointe",
            "04": "Heures de pointe mobile"
        },
        g = {
            "00": "TJ MU",
            "01": "TJ LU",
            "02": "TJ LU-SD",
            "03": "TJ LU-P",
            "04": "TJ LU-PH",
            "05": "TJ LU-CH",
            "06": "TJ EJP",
            "07": "TJ EJP-SD",
            "08": "TJ EJP-PM",
            "09": "TJ EJP-HH",
            "0a": "TV A5 Base",
            "0b": "TV A8 Base",
            "0c": "BT 4 SUP36",
            "0d": "BT 5 SUP36",
            "0e": "HTA 5",
            "0f": "HTA 8"
        },
        y = {
            "00": "P",
            "01": "PM",
            "02": "HH",
            "03": "HP",
            "04": "HC",
            "05": "HPH",
            "06": "HCH",
            "07": "HPE",
            "08": "HCE",
            "09": "HPD",
            "0a": "HCD",
            "0b": "JA"
        },
        k = {
            "0a": "TV A5 Base",
            "0b": "TV A8 Base",
            10: "TV A5 EJP",
            11: "TV A8 EJP",
            12: "TV A8 MOD"
        },
        C = {
            "00": "P",
            "01": "PM",
            "02": "HH",
            "03": "HP",
            "04": "HC",
            "05": "HPH",
            "06": "HCH",
            "07": "HPE",
            "08": "HCE",
            "09": "HPD",
            "0a": "HCD",
            "0b": "JA",
            "0c": "HD",
            "0d": "HM",
            "0e": "DSM",
            "0f": "SCM"
        };
    return this.decode(e, a, t)
};

//console.log(Decoder("000e0081090446104200146c5e08df01", "ambiance_1"))
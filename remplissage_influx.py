from influxdb import InfluxDBClient, DataFrameClient
from datetime import datetime, timedelta
from random import randrange


client_influx = InfluxDBClient(host='localhost', port=8086)
client_influx.switch_database('mobigrid')

dt = datetime(1980, 4, 13, 12, 0, 0)
dt_final = datetime(2022,9,8,13,0,0)
while dt < dt_final:
    json_body = [
                {
                    "measurement": "test",
                    "tags": {
                        "name":  "capteur_1"
                    },
                    "fields": {
                    "data" : 15  
                    } 
                }
            ]
    client_influx.write_points(json_body)
    dt = dt + timedelta(seconds=10)
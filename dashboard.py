import requests
from influxdb import InfluxDBClient, DataFrameClient
from datetime import datetime
import pytz

tz = pytz.timezone('Europe/Paris')

def format(i):
    switch={
        5:"""</tr>
        <tr>""",
        9:"""</tr>
        <tr>""",
        13:"""</tr>
        <tr>""",
        17:"""</tr>
    
</table>

<h2>Capteur Tyness</h2>
<table>
    <tr>""",
        19:"""</tr>
</table>

<h2>Capteur Squid</h2>
<table>
    <tr>""",
        23:"""</tr>
</table>

<h2>Capteur Plug</h2>
<table>
    <tr>"""
        }
    return switch.get(i,"")

client_influx = InfluxDBClient(host='localhost', port=8086, username='admin', password='admin')
client_influx.switch_database('mobigrid')
results = client_influx.query('SELECT "name" FROM "capteur_code"')
names = list(results.get_points(measurement='capteur_code'))
dicHour = {
    "ambiance-1": "inconnue",
    "ambiance-2": "inconnue",
    "ambiance-3": "inconnue",
    "ambiance-4": "inconnue",
    "ambiance-5": "inconnue",
    "ambiance-6": "inconnue",
    "ambiance-7": "inconnue",
    "ambiance-8": "inconnue",
    "ambiance-9": "inconnue",
    "ambiance-10": "inconnue",
    "ambiance-11": "inconnue",
    "ambiance-12": "inconnue",
    "ambiance-13": "inconnue",
    "ambiance-14": "inconnue",
    "ambiance-15": "inconnue",
    "ambiance-16": "inconnue",
    "tyness-21": "inconnue",
    "tyness-22": "inconnue",
    "squid-31": "inconnue",
    "squid-32": "inconnue",
    "squid-33": "inconnue",
    "squid-34": "inconnue",
    "plug-41": "inconnue",
    "plug-42": "inconnue",
    "plug-43": "inconnue",
    "plug-44": "inconnue"
}
dicDate = {
    "ambiance-1": "inconnue",
    "ambiance-2": "inconnue",
    "ambiance-3": "inconnue",
    "ambiance-4": "inconnue",
    "ambiance-5": "inconnue",
    "ambiance-6": "inconnue",
    "ambiance-7": "inconnue",
    "ambiance-8": "inconnue",
    "ambiance-9": "inconnue",
    "ambiance-10": "inconnue",
    "ambiance-11": "inconnue",
    "ambiance-12": "inconnue",
    "ambiance-13": "inconnue",
    "ambiance-14": "inconnue",
    "ambiance-15": "inconnue",
    "ambiance-16": "inconnue",
    "tyness-21": "inconnue",
    "tyness-22": "inconnue",
    "squid-31": "inconnue",
    "squid-32": "inconnue",
    "squid-33": "inconnue",
    "squid-34": "inconnue",
    "plug-41": "inconnue",
    "plug-42": "inconnue",
    "plug-43": "inconnue",
    "plug-44": "inconnue"
}
dicDay = {
    "ambiance-1": "inconnue",
    "ambiance-2": "inconnue",
    "ambiance-3": "inconnue",
    "ambiance-4": "inconnue",
    "ambiance-5": "inconnue",
    "ambiance-6": "inconnue",
    "ambiance-7": "inconnue",
    "ambiance-8": "inconnue",
    "ambiance-9": "inconnue",
    "ambiance-10": "inconnue",
    "ambiance-11": "inconnue",
    "ambiance-12": "inconnue",
    "ambiance-13": "inconnue",
    "ambiance-14": "inconnue",
    "ambiance-15": "inconnue",
    "ambiance-16": "inconnue",
    "tyness-21": "inconnue",
    "tyness-22": "inconnue",
    "squid-31": "inconnue",
    "squid-32": "inconnue",
    "squid-33": "inconnue",
    "squid-34": "inconnue",
    "plug-41": "inconnue",
    "plug-42": "inconnue",
    "plug-43": "inconnue",
    "plug-44": "inconnue"
}

dicWeek = {
    "ambiance-1": "inconnue",
    "ambiance-2": "inconnue",
    "ambiance-3": "inconnue",
    "ambiance-4": "inconnue",
    "ambiance-5": "inconnue",
    "ambiance-6": "inconnue",
    "ambiance-7": "inconnue",
    "ambiance-8": "inconnue",
    "ambiance-9": "inconnue",
    "ambiance-10": "inconnue",
    "ambiance-11": "inconnue",
    "ambiance-12": "inconnue",
    "ambiance-13": "inconnue",
    "ambiance-14": "inconnue",
    "ambiance-15": "inconnue",
    "ambiance-16": "inconnue",
    "tyness-21": "inconnue",
    "tyness-22": "inconnue",
    "squid-31": "inconnue",
    "squid-32": "inconnue",
    "squid-33": "inconnue",
    "squid-34": "inconnue",
    "plug-41": "inconnue",
    "plug-42": "inconnue",
    "plug-43": "inconnue",
    "plug-44": "inconnue"
}

f = open('dashboard.html', 'w')
message ="""
<!DOCTYPE html>
<html>
    <style>
        h1 {
            text-align:center;
        }
        h2 {
            margin-left:3%;
        }

        h4 {
            text-align:right;
        }
        table {
            width: 100%;
            margin-bottom: 5%;
            text-align:center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        table#mytable,
        table#mytable td{
            border: none !important;
        }
        .red {
            background-color: red;
        }
        .yellow {
            background-color: yellow;
        }
    </style>
<body>
<h1>Tableau de Bord</h1>
<h4>""" + datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S') +"""</h4>
<h2>Capteur Ambiance</h2>
<table>
    <tr>"""
i = 0
for nameDic in names:
    i = i + 1
    name = nameDic["name"]
    lastData = list(client_influx.query('select "name", "date" from data where "name" = ' + "'" + name + "'" + ' order by time desc limit 1'))
    lastHour = list(client_influx.query('select count("date") from data where "name" = ' + "'" + name + "'" + 'and time > now() - 1h'))
    lastDay = list(client_influx.query('select count("date") from data where "name" = ' + "'" + name + "'" + 'and time > now() - 1d'))
    lastWeek = list(client_influx.query('select count("date") from data where "name" = ' + "'" + name + "'" + 'and time > now() - 7d'))
    #print(lastData)
    try:
        dicDate[name] = lastData[0][0]['date']
    except:
        dicDate[name] = "unknow"
    try:
        dicHour[name] = lastHour[0][0]['count']
    except:
         dicHour[name] = 0
    try:
        dicDay[name] = lastDay[0][0]['count']
    except:
         dicDay[name] = 0
    try:
        dicWeek[name] = lastWeek[0][0]['count']
    except:
         dicWeek[name] = 0
    

    contenu = format(i)

    ratio = 10
    if "plug" in name:
        ratio = 1

    """couleur = ""
    if dicBatterie[name] == "niveau critique":
        couleur = 'class="red"'
    elif dicBatterie[name] == "niveau tres faible" or dicBatterie[name] == "niveau faible":
        couleur = 'class="yellow"'
    """ 
    contenu = contenu + """
        <td>
            <h3>""" + name + """</h3>
            
                <p>date = """ + dicDate[name] + """</p>
                <table id="mytable">
                    <tr>
                        <td><p>Last Hour = """ + str(dicHour[name]) + """ / """ + str(60/ratio) + """</p></td><td><p>""" + str(round((dicHour[name]/(60/ratio) * 100), 2)) + """%</p></td></tr>
                        <td><p>Last Day = """ + str(dicDay[name]) + """ / """ + str(60*24/ratio) + """</p></td><td><p>""" + str(round((dicDay[name]/(60*24/ratio) * 100), 2)) + """%</p></td></tr>
                        <td><p>Last Week = """ + str(dicWeek[name]) + """ / """ + str(60*24*7/ratio) + """</p></td><td><p>""" + str(round((dicWeek[name]/(60*24*7/ratio) * 100), 2)) + """%</p></td></tr>
                </table>
        </td>
    """
    if i == 26:
        contenu = contenu + """</tr>
</table>

</body>
</html> """
    message = message + contenu

f.write(message)
f.close()


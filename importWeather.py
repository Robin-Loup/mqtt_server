from influxdb import InfluxDBClient, DataFrameClient
from datetime import datetime, timedelta
import pandas as pd


client_influx = InfluxDBClient(host='localhost', port=8086, username='admin', password='admin')
client_influx.switch_database('mobigrid')


df = pd.read_csv("dataweather_1.csv")
#print(df)
for idx, line in df.iterrows():
    #print(line)
    values = {}
    values["date"] = line["date"]
    temperature = 'value00'

    humidity = 'value02'
    wind_deg = 'value03'
    wind_speed = 'value04'

    values[temperature] = line['temperature']
    values[humidity] = int(line['humidity'])
    values[wind_deg] = line['wind_deg']
    values[wind_speed] = line['wind_speed']


    results = client_influx.query('SELECT "id" FROM "weather_code" WHERE "main" = \''+line['weather'] + '\'')   
    if(results):
        weatherId = list(results.get_points(measurement='weather_code'))[0]["id"]
    values['value01'] = str(weatherId)


    json_body = [
                {
                    "measurement": "data",
                    "tags": {
                        "name":  line["name"]
                    },
                    "time": line["time"],
                    "fields": values
                }
            ]
    #print(json_body)
    client_influx.write_points(json_body)

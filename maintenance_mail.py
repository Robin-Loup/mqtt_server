import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText


# on rentre les renseignements pris sur le site du fournisseur
smtp_address = 'smtp.gmail.com'
smtp_port = 465

# on rentre les informations sur notre adresse e-mail
email_address = 'maintenance.transpolis@gmail.com'
email_password = 'tr4sp0l1ce'

# on rentre les informations sur le destinataire
email_receiver = 'robin.rieutord@univ-eiffel.fr'

fileToSend1 = "/home/liris/code/mqtt_server/dashboard.html"
fileToSend2 = "/home/liris/code/mqtt_server/data_recovery.log"


# on crée un e-mail
message = MIMEMultipart("alternative")
# on ajoute un sujet
message["Subject"] = "e-mail essai"
# un émetteur
message["From"] = email_address
# un destinataire
message["To"] = email_receiver

# on crée un texte et sa version HTML
texte = '''
Maintenance Transpolis
'''

html = '''
<html>
<body>
<p>Maintenance Transpolis</p>
</body>
</html>
'''

# on crée deux éléments MIMEText 
texte_mime = MIMEText(texte, 'plain')
html_mime = MIMEText(html, 'html')

# on attache ces deux éléments 
message.attach(texte_mime)
message.attach(html_mime)

ctype, encoding = mimetypes.guess_type(fileToSend1)
if ctype is None or encoding is not None:
    ctype = "application/octet-stream"

maintype, subtype = ctype.split("/", 1)

if maintype == "text":
    fp = open(fileToSend1)
    # Note: we should handle calculating the charset
    attachment = MIMEText(fp.read(), _subtype=subtype)
    fp.close()
else:
   if maintype == "image":
      fp = open(fileToSend1, "rb")
      attachment = MIMEImage(fp.read(), _subtype=subtype)
      fp.close()
   else:
      if maintype == "audio":
         fp = open(fileToSend1, "rb")
         attachment = MIMEAudio(fp.read(), _subtype=subtype)
         fp.close()
      else:
         fp = open(fileToSend1, "rb")
         attachment = MIMEBase(maintype, subtype)
         attachment.set_payload(fp.read())
         fp.close()
         encoders.encode_base64(attachment)
attachment.add_header("Content-Disposition", "attachment", filename=fileToSend1)
message.attach(attachment)


ctype, encoding = mimetypes.guess_type(fileToSend2)
if ctype is None or encoding is not None:
    ctype = "application/octet-stream"

maintype, subtype = ctype.split("/", 1)

if maintype == "text":
    fp = open(fileToSend2)
    # Note: we should handle calculating the charset
    attachment = MIMEText(fp.read(), _subtype=subtype)
    fp.close()
else:
   if maintype == "image":
      fp = open(fileToSend2, "rb")
      attachment = MIMEImage(fp.read(), _subtype=subtype)
      fp.close()
   else:
      if maintype == "audio":
         fp = open(fileToSend2, "rb")
         attachment = MIMEAudio(fp.read(), _subtype=subtype)
         fp.close()
      else:
         fp = open(fileToSend2, "rb")
         attachment = MIMEBase(maintype, subtype)
         attachment.set_payload(fp.read())
         fp.close()
         encoders.encode_base64(attachment)
attachment.add_header("Content-Disposition", "attachment", filename=fileToSend2)
message.attach(attachment)



server = smtplib.SMTP("smtp.gmail.com:587")
server.starttls()
server.login(email_address,email_password)
server.sendmail(email_address, email_receiver, message.as_string())
server.quit()

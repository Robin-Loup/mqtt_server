import json
from influxdb import InfluxDBClient, DataFrameClient
import requests
from datetime import datetime
import pytz
import execjs
execjs.get().name
'Node.js (V8)'

# Ce script a pour objectif de stockée les données brut des capteurs transpolis dans la base de donnée Influx

#time zone utc
tz = pytz.timezone('Europe/Paris')

#fonction pour ecrire dans fichier log
def write_file(message):
    file = open ("data_recovery2.log", "a")
    file.write(message + "\n")
    file.close


def on_connect(client_mqtt, userdata, flags, rc):
    #print("Connected with result code "+str(rc))
    file = open("data_recovery2.log", "w")
    file.close
    client_mqtt.subscribe("#")
    write_file("on connect with result code "+ str(rc))

def on_message(client_mqtt, userdata, msg):
    #write_file("on message")
  
    #Recuperer name
    x = msg.topic.split("/")
    client_influx = InfluxDBClient(host='localhost', port=8086, username='admin', password='admin')
    client_influx.switch_database('mobigrid')


    results = client_influx.query('SELECT "name" FROM "capteur_code" WHERE "deveui" = \'' +x[2] + '\'')
    
    if(results):
        name = list(results.get_points(measurement='capteur_code'))[0]["name"]

        try:
            payload = dataCapteur["data"]
            
        except:
            write_file(name)
            write_file(datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))
            write_file(payload)
            write_file("error: payload")
            write_file("")


        values = {}        
        if name.find("plug")!= -1:
            try:
                #write_file("decode payload plug")
                with open("nke.js") as n:
                    ctnke =execjs.compile(n.read())
                data = ctnke.call('DecoderToString',payload)
                #print(data)
                for idx, value in enumerate(data['data']) :
                    valname = 'value'+str(idx).zfill(2)
                    values[valname] = float(data['data'][value])
            except:
                write_file(name)
                write_file(datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))
                write_file(payload)
                write_file("error: decode payload plug")
                #write_file(data)
                write_file("")
        else:
            if name.find("tyness-21")!= -1:
                #Tyness-21
                try:
                    #write_file("decode payload tyness-21")
                    with open("ewattch.js") as f:
                        ctewattch =execjs.compile(f.read())
                    data = ctewattch.call('DecoderToString',payload)

                    for idx, value in enumerate(data['data'][0]['values']) :
                        valname = 'value'+str(idx).zfill(2)
                        values[valname] = float(value)
                
                except:
                    write_file(name)
                    write_file(datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))
                    write_file(payload)
                    write_file("error: decode payload tyness-21")
                    write_file("")
            
            else:
                try:
                    with open("ewattch.js") as f:
                        ctewattch =execjs.compile(f.read())
                    data = ctewattch.call('DecoderToString',payload)
    
                   
                    for idx, value in enumerate(data['data']) :
                        valname = 'value'+str(idx).zfill(2)
                        values[valname] = float(value['value'])
                    
                except:
                    write_file(name)
                    write_file(datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))
                    write_file(payload)
                    write_file("error: decode payload")
                    write_file("")

        values["date"] = datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S')

        #creation du Json
        try:
            json_body = [
                {
                    "measurement": "data",
                    "tags": {
                        "name":  name
                    },
                    "fields": values
                }
            ]
        except:
            write_file(name)
            write_file(datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))
            write_file(payload)
            write_file("impossible to create json_body of data sensor")
            write_file("")

         #stockage du json dataSensor       
    
        try:
            client_influx.write_points(json_body)
        except:
            print(json_body)
            write_file("error : client_influx.write_points(json_body) of data sensor")
            #write_file(json_body)

    else:
        #deveui non trouvé dans la liste de capteur
        write_file("error: " + x[2] + " not found")
    """write_file("off message")
    write_file("")"""

    """client.disconnect()"""



file = open ("data_recovery.log", "w")
file.write("")
file.close

client_mqtt = mqtt.Client()
client_mqtt.username_pw_set(username="transpolis",password="084084")
client_mqtt.connect("localhost",1883,60)

client_mqtt.on_connect = on_connect
client_mqtt.on_message = on_message
write_file("start loop")
client_mqtt.loop_forever()
write_file("end loop")



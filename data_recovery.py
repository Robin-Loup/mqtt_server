import json
from influxdb import InfluxDBClient, DataFrameClient
import requests
import paho.mqtt.client as mqtt
from datetime import datetime
import pytz

# Ce script a pour objectif de stockée les données brut des capteurs transpolis dans la base de donnée Influx

#time zone utc
tz = pytz.timezone('Europe/Paris')

# Creation de l'API pour les donnée météo sur openweathermap
api_key = "1c7f7e52eab821f3026f622afd815dbb"
lat = "45.934789687339766"
lon = "5.267146714587412"
url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (lat, lon, api_key)

#fonction pour ecrire dans fichier log
def write_file(message):
    file = open ("data_recovery.log", "a")
    file.write(message + "\n")
    file.close


def on_connect(client_mqtt, userdata, flags, rc):
    #print("Connected with result code "+str(rc))
    client_mqtt.subscribe("#")
    write_file("on connect with result code "+ str(rc))

def on_message(client_mqtt, userdata, msg):
    write_file("on message")
  
    #Recuperer name
    x = msg.topic.split("/")
    client_influx = InfluxDBClient(host='localhost', port=8086)
    client_influx.switch_database('mobigrid')


    results = client_influx.query('SELECT "name" FROM "capteur_code" WHERE "deveui" = \'' +x[2] + '\'')
    
    if(results):
        name = list(results.get_points(measurement='capteur_code'))[0]["name"]
      

        #recuperer payload
        try:
            dataCapteur = json.loads(msg.payload.decode())
            write_file(name)
        except:
            write_file("error: json.loads(msg.payload.decode())")

        try:
            payload = dataCapteur["data"]
            write_file(payload)
        except:
            write_file("error: payload")
    
        #creation du Json
        json_body = [
            {
                "measurement": "capteur_data",
                "tags": {
                    "date":  datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S')
                },
                "fields": {
                    "name": name,
                    "data" : payload         
                }
            }
        ]

        #stockage du json dataSensor
        try:
            client_influx.write_points(json_body)
            write_file(datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))
            write_file("client_influx.write_points(json_body) of data sensor")
        except:
            write_file("error : client_influx.write_points(json_body) of data sensor")
            #write_file(json_body)

        
        #weather
        try:
            response = requests.get(url)
        except:
            write_file("error: url wrong")

        try:
            data = json.loads(response.text)
            temperature = data["current"]["temp"]
            weather = data["current"]["weather"][0]["main"]
            humidity = data["current"]["humidity"]
            wind_speed = data["current"]["wind_speed"]
            wind_deg = data["current"]["wind_deg"]
                        
            json_body = [
                {
                    "measurement": "weather",
                    "tags": {
                        "weather": weather,
                        "date":  datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S')
                    },
                    "fields": {
                        "humidity": humidity,
                        "wind_speed": wind_speed,
                        "wind_deg": wind_deg,
                        "temperature": temperature
                    }
                }
            ]
        except:
             write_file("impossible to create json_body of data weather")
        #stockage json weather
        try:
            client_influx.write_points(json_body)
            write_file("client_influx.write_points(json_body) of data weather")
        except:
            write_file("error : client_influx.write_points(json_body) of data weather")
            #write_file(json_body)

    else:
        #deveui non trouvé dans la liste de capteur
        write_file("error: " + x[2] + " not found")
    write_file("off message")
    write_file("")

    """client.disconnect()"""



file = open ("data_recovery.log", "w")
file.write("")
file.close

client_mqtt = mqtt.Client()
client_mqtt.username_pw_set(username="transpolis",password="084084")
client_mqtt.connect("localhost",1883,60)

client_mqtt.on_connect = on_connect
client_mqtt.on_message = on_message
write_file("start loop")
client_mqtt.loop_forever()
write_file("end loop")



import json
from influxdb import InfluxDBClient, DataFrameClient

client = InfluxDBClient(host='localhost', port=8086)
client.switch_database('mobigrid')
client.query('DELETE FROM "weather_code"')

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "200",
            "main": "Thunderstorm",
            "description": "thunderstorm with light rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "201",
            "main": "Thunderstorm",
            "description": "thunderstorm with rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "202",
            "main": "Thunderstorm",
            "description": "thunderstorm with heavy rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "210",
            "main": "Thunderstorm",
            "description": "light thunderstorm"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "211",
            "main": "Thunderstorm",
            "description": "thunderstorm"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "212",
            "main": "Thunderstorm",
            "description": "heavy thunderstorm"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "221",
            "main": "Thunderstorm",
            "description": "ragged thunderstorm"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "230",
            "main": "Thunderstorm",
            "description": "thunderstorm with light drizzle"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "231",
            "main": "Thunderstorm",
            "description": "thunderstorm with drizzle"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "232",
            "main": "Thunderstorm",
            "description": "thunderstorm with heavy drizzle"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "300",
            "main": "Drizzle",
            "description": "light intensity drizzle"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "301",
            "main": "Drizzle",
            "description": "drizzle"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "302",
            "main": "Drizzle",
            "description": "heavy intensity drizzle"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "310",
            "main": "Drizzle",
            "description": "light intensity drizzle rain"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "311",
            "main": "Drizzle",
            "description": "drizzle rain"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "312",
            "main": "Drizzle",
            "description": "heavy intensity drizzle rain"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "313",
            "main": "Drizzle",
            "description": "shower rain and drizzle"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "314",
            "main": "Drizzle",
            "description": "heavy shower rain and drizzle"
        }
    }
]
client.write_points(json_body)


json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "321",
            "main": "Drizzle",
            "description": "shower drizzle"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "500",
            "main": "Rain",
            "description": "light rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "501",
            "main": "Rain",
            "description": "moderate rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "502",
            "main": "Rain",
            "description": "heavy intensity rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "503",
            "main": "Rain",
            "description": "very heavy rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "504",
            "main": "Rain",
            "description": "extreme rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "511",
            "main": "Rain",
            "description": "freezing rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "520",
            "main": "Rain",
            "description": "light intensity shower rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "521",
            "main": "Rain",
            "description": "shower rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "522",
            "main": "Rain",
            "description": "heavy intensity shower rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "531",
            "main": "Rain",
            "description": "ragged shower rain"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "600",
            "main": "Snow",
            "description": "light snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "601",
            "main": "Snow",
            "description": "Snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "602",
            "main": "Snow",
            "description": "Heavy snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "611",
            "main": "Snow",
            "description": "Sleet"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "612",
            "main": "Snow",
            "description": "Light shower sleet"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "613",
            "main": "Snow",
            "description": "Shower sleet"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "615",
            "main": "Snow",
            "description": "Light rain and snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "616",
            "main": "Snow",
            "description": "Rain and snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "620",
            "main": "Snow",
            "description": "Light shower snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "621",
            "main": "Snow",
            "description": "Shower snow "
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "622",
            "main": "Snow",
            "description": "Heavy shower snow"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "701",
            "main": "Mist",
            "description": "mist"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "711",
            "main": "Smoke",
            "description": "Smoke"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "721",
            "main": "Haze",
            "description": "Haze"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "731",
            "main": "Dust",
            "description": "sand/ dust whirls"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "741",
            "main": "Fog",
            "description": "fog"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "751",
            "main": "Sand",
            "description": "sand"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "761",
            "main": "Dust",
            "description": "dust"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "762",
            "main": "Ash",
            "description": "volcanic ash"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "771",
            "main": "Squall",
            "description": "squalls"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "781",
            "main": "Tornado",
            "description": "tornado"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "800",
            "main": "Clear",
            "description": "clear sky"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "801",
            "main": "Clouds",
            "description": "few clouds: 11-25%"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "802",
            "main": "Clouds",
            "description": "scattered clouds: 25-50%"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "803",
            "main": "Clouds",
            "description": "broken clouds: 51-84%"
        }
    }
]
client.write_points(json_body)

json_body = [
    {
        "measurement": "weather_code",
        "fields": {
            "id": "804",
            "main": "Clouds",
            "description": "overcast clouds: 85-100%"
        }
    }
]
client.write_points(json_body)
import json
from influxdb import InfluxDBClient, DataFrameClient
import requests
import pytz
from datetime import datetime

tz = pytz.timezone('Europe/Paris')

# Creation de l'API pour les donnée météo sur openweathermap
api_key = "1c7f7e52eab821f3026f622afd815dbb"
lat = "45.934789687339766"
lon = "5.267146714587412"
url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s&units=metric" % (lat, lon, api_key)

#fonction pour ecrire dans fichier log
def write_file(message):
    file = open ("weather_collect.log", "a")
    file.write(message + "\n")
    file.close


client_influx = InfluxDBClient(host='localhost', port=8086, username='admin', password='admin')

client_influx.switch_database('mobigrid')

try:
    response = requests.get(url)
except:
    write_file("error: url wrong")

try:
    data = json.loads(response.text)
    values = {}
    values['value00'] = float(data["current"]["temp"])
    values['value01'] = float(data["current"]["weather"][0]["id"])
    values['value02'] = float(data["current"]["humidity"])
    values['value03'] = float(data["current"]["wind_speed"])
    values['value04'] = float(data["current"]["wind_deg"])
    values["date"] = datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S')

    """temperature = data["current"]["temp"]
    weather = data["current"]["weather"][0]["main"]
    humidity = data["current"]["humidity"]
    wind_speed = data["current"]["wind_speed"]
    wind_deg = data["current"]["wind_deg"]"""
                
    json_body = [
        {
            "measurement": "data",
            "tags": {
                "name":  "weather"
            },
            "fields": values
        }
    ]

    #print(values)
except:
    #print("fail")
    write_file("impossible to create json_body of data weather")
#stockage json weather
try:
    client_influx.write_points(json_body)
    #print("ok")
    #write_file("client_influx.write_points(json_body) of data weather")
except:
    #print("fail")
    write_file("error : client_influx.write_points(json_body) of data weather")
    #write_file(json_body)

from influxdb import InfluxDBClient, DataFrameClient
from datetime import datetime, timedelta
import pandas as pd


client_influx = InfluxDBClient(host='localhost', port=8086)
client_influx.switch_database('mobigrid')
df = pd.read_csv("~/Documents/Server_MQTT/mqtt_server/data/data_1.csv")

print(df)
print(df["name"][0])
print(df["time"][0])
timing = (float(df["time"][0]))
json_body = [
                {
                    "measurement": "test_import",
                    "tags": {
                        "name":  df["name"][0]
                    },
                    "time": df["time"][0],
                    "fields": {
                        "data" : 15  
                    } 
                }
            ]
#client_influx.write_points(json_body)